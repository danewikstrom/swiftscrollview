//
//  ViewController.swift
//  ScrollViewSwift
//
//  Created by Dane Wikstrom on 9/6/17.
//  Copyright © 2017 Dane Wikstrom. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad()
    {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        let scrollView = UIScrollView(frame: self.view.bounds)
        self.view.addSubview(scrollView)
        scrollView.isPagingEnabled = true
        scrollView.contentSize = CGSize(width: self.view.bounds.size.width * 3, height: self.view.bounds.size.height * 3) //9 squares
        
        let view1 = UIView(frame: CGRect(x: 0, y: 0, width: self.view.bounds.size.width, height: self.view.bounds.size.height))//0,0
        view1.backgroundColor = UIColor.red
        scrollView.addSubview(view1)
        
        let view2 = UIView(frame: CGRect(x: self.view.bounds.size.width, y: 0, width: self.view.bounds.size.width, height: self.view.bounds.size.height))//0,1
        view2.backgroundColor = UIColor.white
        scrollView.addSubview(view2)
        
        let view3 = UIView(frame: CGRect(x: self.view.bounds.size.width * 2, y: 0, width: self.view.bounds.size.width, height: self.view.bounds.size.height))//0,2
        view3.backgroundColor = UIColor.blue
        scrollView.addSubview(view3)
        
        let view4 = UIView(frame: CGRect(x: 0, y: self.view.bounds.size.height, width: self.view.bounds.size.width, height: self.view.bounds.size.height))//1,0
        view4.backgroundColor = UIColor.purple
        scrollView.addSubview(view4)
        
        let view5 = UIView(frame: CGRect(x: self.view.bounds.size.width, y: self.view.bounds.size.height, width: self.view.bounds.size.width, height: self.view.bounds.size.height))//1,1
        view5.backgroundColor = UIColor.black
        scrollView.addSubview(view5)
        
        let view6 = UIView(frame: CGRect(x: self.view.bounds.size.width * 2, y: self.view.bounds.size.height, width: self.view.bounds.size.width, height: self.view.bounds.size.height))//1,2
        view6.backgroundColor = UIColor.magenta
        scrollView.addSubview(view6)
        
        let view7 = UIView(frame: CGRect(x: 0, y: self.view.bounds.size.height * 2, width: self.view.bounds.size.height, height: self.view.bounds.size.height))//2,0
        view7.backgroundColor = UIColor.orange
        scrollView.addSubview(view7)
        
        let view8 = UIView(frame: CGRect(x: self.view.bounds.size.width, y: self.view.bounds.size.height * 2, width: self.view.bounds.size.height, height: self.view.bounds.size.height))//2,1
        view8.backgroundColor = UIColor.green
        scrollView.addSubview(view8)
        
        let view9 = UIView(frame: CGRect(x: self.view.bounds.size.width * 2, y: self.view.bounds.size.height * 2, width: self.view.bounds.size.width, height: self.view.bounds.size.height))//2,2
        view9.backgroundColor = UIColor.cyan
        scrollView.addSubview(view9)
    }

    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

